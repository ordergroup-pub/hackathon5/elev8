# React template repository

This project is a base template for creating React applications. It includes the following features:

- Create react app @5.0.1
- Typescript @4.8.4
- React Router @6.4.2
- i18next @21.10.0

### Project requirements

1. Docker
2. Docker-compose
3. Yarn
4. Node v.16+

## Template usage:

In order to configure the React project based on this template, you need to run the following commands:

1. Create new repository on remote server (GitHub, GitLab, etc.)
2. Open terminal and run the following commands:

```bash
    git clone https://gitlab.com/ordergroupco/react-typescript-boilerplate.git your_new_repo
```

```bash
    cd your_new_repo
```

```bash
    git remote set-url origin https://gitlab.com/user.name/your_new_repo.git
```
```bash
    git branch dev
```
```bash
    git checkout dev
```
```bash
    git push
```
3. Your new repository is ready to use. You can now create merge request from dev to main branch.


# Project run:

By default, the docker image has a `react-template` name. If you want to change it, you need to change the name in the
`docker-compose.yml` file.

## Docker run

### Docker Production

```bash
    docker-compose -f docker-compose.prod.yml up -d --build
```

### Docker Development

```bash
    docker-compose up -d --build
```

Docker build and run project on the address `http://localhost:3000`.

## Manual run

### React service (local):

1. `cp .env.local .env` - move env file to root folder.
2. `yarn install` - to install all the required dependencies.
3. `yarn start` - to start the server.
4. `yarn build` - to build the project
