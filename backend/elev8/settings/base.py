"""
Django settings for elev8 project.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""
import os

from utils.fargate import get_fargate_container_ip
from utils.settings import get_env, get_env_bool, get_env_int

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT_INDIRECT = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../../")
PROJECT_ROOT = os.path.realpath(PROJECT_ROOT_INDIRECT)
FILE_SERVING_ROOT = os.path.join(PROJECT_ROOT, "public")
PRIVATE_FILE_SERVING_ROOT = os.path.join(PROJECT_ROOT, "private")
PRIVATE_URL = "/private/"
SCRIPT_OUTPUT_PATH = os.path.join(PROJECT_ROOT, "output")

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-bd4w6j3mlu6+&#xdo4am(2$#_@@-$2sdk9b$++0m6sstd9&aj7"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
DEBUG_SILK = False

external_hosts = [host for host in get_env("ALLOWED_HOSTS", default="").split(",") if host]

APP_URL = get_env("APP_URL", default="localhost:8000")
if APP_URL:
    external_hosts.append(APP_URL)

BASE_SITE_URL = f"https://{APP_URL}"

ALLOWED_HOSTS = ["localhost", "127.0.0.1"] + external_hosts


ADD_FARGATE_IP = get_env_bool("ADD_DOCKER_IP", default=False)
if ADD_FARGATE_IP:
    DOCKER_PRIVATE_IP = get_fargate_container_ip()
    if DOCKER_PRIVATE_IP:
        ALLOWED_HOSTS.append(DOCKER_PRIVATE_IP)


INTERNAL_IPS = ("127.0.0.1",)

SILENCED_SYSTEM_CHECKS = ["admin.E115"]

# Application definition
INSTALLED_APPS = [
    "dal",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "storages",
    "anymail",
    "admin_auto_filters",
    "django_extensions",
    "og_django_utils",
    "django_object_actions",
    "django_json_widget",
    "rangefilter",
    "health_check",
    "health_check.db",
    "health_check.cache",
    "mathfilters",
    "solo",
    "django_admin_inline_paginator",
    "nested_admin",
    "rest_framework",
    "rest_framework.authtoken",
    "rest_framework_simplejwt",
    "corsheaders",
]

PROJECT_APPS = [
    "elev8",
    "utils",
]

INSTALLED_APPS += PROJECT_APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "og_django_utils.utils.middleware.CriticalLogMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "elev8.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(PROJECT_ROOT, "templates"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "elev8.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": get_env("DATABASE_NAME"),
        "USER": get_env("DATABASE_USER"),
        "PASSWORD": get_env("DATABASE_PASSWORD"),
        "HOST": get_env("DATABASE_HOST"),
        "PORT": get_env("DATABASE_PORT", default=5432),
    }
}

# REST
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    # 'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    "DEFAULT_AUTHENTICATION_CLASSES": ("rest_framework_simplejwt.authentication.JWTAuthentication",),
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},  # NOQA
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},  # NOQA
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},  # NOQA
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},  # NOQA
]

DATA_UPLOAD_MAX_NUMBER_FIELDS = 5000

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = "en-us"
TIME_ZONE = "Europe/Warsaw"
USE_I18N = True
USE_L10N = False
USE_TZ = True
DATETIME_FORMAT = "Y-m-d H:i:s:u"


AWS_DEFAULT_REGION = get_env("AWS_DEFAULT_REGION", "eu-west-1")
AWS_ECS_SESSION = get_env("AWS_ECS_SESSION", False)
if not AWS_ECS_SESSION:
    AWS_ACCESS_KEY_ID = get_env("AWS_ACCESS_KEY_ID", None)
    AWS_SECRET_ACCESS_KEY = get_env("AWS_SECRET_ACCESS_KEY", None)
else:
    AWS_ACCESS_KEY_ID = None
    AWS_SECRET_ACCESS_KEY = None

AWS_STORAGE_BUCKET_NAME = get_env("AWS_BUCKET_NAME", None)

PRIVATE_MEDIA_LOCATION = "private"
PUBLIC_MEDIA_LOCATION = "media"
STATIC_LOCATION = "static"
LOCAL_MIRROR_LOCATION = "mirrored_files"
MIRRORED_FILES_ROOT = os.path.join(PROJECT_ROOT, LOCAL_MIRROR_LOCATION)
LOCAL_CACHE_LOCATION = "local_cache"
CACHE_FILES_ROOT = os.path.join(PROJECT_ROOT, LOCAL_CACHE_LOCATION)

# STATICFILES_DIRS = (os.path.join(PROJECT_ROOT, "assets"),)

# File storage
if (AWS_ACCESS_KEY_ID or AWS_ECS_SESSION) and AWS_STORAGE_BUCKET_NAME:
    S3_STORAGE_ENABLED = True
    PRIVATE_FILE_STORAGE_ON_S3 = True
    DEFAULT_FILE_STORAGE = "elev8.storage.MediaStorage"
    THUMBNAIL_DEFAULT_STORAGE = "elev8.storage.ThumbnailStorage"
    STATICFILES_STORAGE = "elev8.storage.StaticfilesStorage"

    AWS_S3_FILE_OVERWRITE = False

    AWS_EXPIRY = 60 * 60 * 24 * 7  # One week
    AWS_S3_OBJECT_PARAMETERS = {"CacheControl": "max-age={}".format(AWS_EXPIRY)}
    MEDIA_URL = "https://{}/{}/".format("%s.s3.amazonaws.com" % AWS_STORAGE_BUCKET_NAME, PUBLIC_MEDIA_LOCATION)
    STATIC_URL = "https://{}/{}/".format("%s.s3.amazonaws.com" % AWS_STORAGE_BUCKET_NAME, STATIC_LOCATION)

    COLLECTFAST_STRATEGY = "collectfast.strategies.boto3.Boto3Strategy"
else:
    S3_STORAGE_ENABLED = False
    MEDIA_URL = "/media/"
    MEDIA_ROOT = os.path.join(FILE_SERVING_ROOT, "media")

    STATIC_URL = "/static/"
    STATIC_ROOT = os.path.join(FILE_SERVING_ROOT, "static")
    COLLECTFAST_STRATEGY = "collectfast.strategies.filesystem.FileSystemStrategy"
    DISABLE_COLLECTFAST = True
    STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
    PRIVATE_FILE_STORAGE_ON_S3 = False


FRONTEND_URL = get_env("FRONTEND_URL", default="http://localhost:3000")

REDIS_HOST = os.environ.get("REDIS_HOST", "localhost")
REDIS_DB = 1
