"""
Write prod settings here, or override base settings
"""

from elev8.settings._logging import generate_logging
from elev8.settings.base import *  # NOQA
from utils.settings import get_env

DEBUG = False

DATABASES["default"]["CONN_MAX_AGE"] = get_env("DATABASE_CONN_MAX_AGE", default=60)

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": f"redis://{REDIS_HOST}:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
        "TIMEOUT": 900,
    }
}

# This ensures that Django will be able to detect a secure connection
# properly on Heroku.
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# Prevent Man in the middle attacks with HTTP Strict Transport Security
SECURE_HSTS_SECONDS = 31536000
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True

# Block content that appears to be an XSS attack in certain browsers
SECURE_BROWSER_XSS_FILTER = True

# Use a secure cookie for the session cookie
SESSION_COOKIE_SECURE = True

# Use a secure cookie for the CSRF cookie
CSRF_COOKIE_SECURE = True

LOG_DIRECTORY = get_env("LOGS_PATH", os.path.join(PROJECT_ROOT, "logs"))


LOGGING = generate_logging(LOG_DIRECTORY, use_cloudwatch=False, disable_local_logs=False, debug_sql=False)

CORS_ALLOWED_ORIGINS = [
    f"{FRONTEND_URL}",
]
