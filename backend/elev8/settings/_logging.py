# -*- coding: utf-8 -*-
import os

LOCAL_LOGFILES_MAP = {
    # app_name: filename
    "general": "general",
    "users": "users",
    "critical": "critical",
    "mailing": "mailing",
    "celery": "celery",
    "health_check": "health_check",
    "cache": "cache",
    "testing": "testing",
    "staticfiles": "staticfiles",
}


def generate_logging(
    log_directory,
    use_cloudwatch=False,
    boto3_session=None,
    cloudwatch_suffix="",
    disable_local_logs=False,
    debug_sql=False,
):
    logfile_size = 5 * 1024 * 1024
    logfile_count = 10

    skip_logging = os.environ.get("DISABLE_LOGGING", False)
    if skip_logging:
        print("Disabled all logger definitions!")
        return {}

    def generate_file_handler(filename):
        return {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(log_directory, f"{filename}.log"),
            "formatter": "verbose",
            "maxBytes": logfile_size,
            "backupCount": logfile_count,
        }

    handlers = {
        "null": {
            "level": "DEBUG",
            "class": "logging.NullHandler",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
        },
    }
    if not disable_local_logs:
        for handler_name, filename in LOCAL_LOGFILES_MAP.items():
            handlers[handler_name] = generate_file_handler(filename)

    additional_handlers = []
    log_group = "elev8_service/app_logs"
    if cloudwatch_suffix:
        log_group += f"-{cloudwatch_suffix}"
    if use_cloudwatch:
        handlers["watchtower"] = {
            "level": "DEBUG",
            "class": "watchtower.CloudWatchLogHandler",
            "boto3_session": boto3_session,
            "log_group": log_group,
        }
        additional_handlers.append("watchtower")
    logging_conf = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "verbose": {"format": "[%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d] %(message)s"},
            "simple": {"format": "[%(levelname)s %(asctime)s] %(message)s"},
            "aws": {
                # you can add specific format for aws here
                "format": "%(asctime)s [%(levelname)-8s] %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": handlers,
        "loggers": {
            "general": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "users": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "critical": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "mailing": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "health_check": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "cache": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "testing": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            "staticfiles": {
                "handlers": ["console"] + additional_handlers,
                "level": "DEBUG",
                "propagate": True,
            },
            # Only for debugging
            "django.db.backends": {
                "handlers": ["console"],
                "level": "DEBUG",
                "propagate": True,
            },
        },
    }
    if not debug_sql:
        logging_conf["loggers"].pop("django.db.backends", None)
    if not disable_local_logs:
        for handler_name, logger_dict in logging_conf["loggers"].items():
            if handler_name in handlers:
                logger_dict["handlers"].insert(0, handler_name)
    return logging_conf
