# -*- coding: utf-8 -*-

from django.apps import AppConfig


class DefaultAppConfig(AppConfig):
    name = "elev8"
