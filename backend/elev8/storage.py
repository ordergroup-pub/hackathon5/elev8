# -*- coding: utf-8 -*-
import os
from abc import ABC

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from storages.backends.s3boto3 import S3Boto3Storage


class MediaStorage(S3Boto3Storage, ABC):
    location = settings.PUBLIC_MEDIA_LOCATION
    file_overwrite = False
    object_parameters = {"CacheControl": "max-age=31536000"}
    querystring_auth = False


class ThumbnailStorage(MediaStorage, ABC):
    file_overwrite = True


class PrivateLocalFileStorage(FileSystemStorage):
    """
    Overwritten local file storage that makes files private
    """

    location = settings.PRIVATE_FILE_SERVING_ROOT
    base_url = settings.PRIVATE_URL


class PrivateS3MediaStorage(S3Boto3Storage, ABC):
    location = settings.PRIVATE_MEDIA_LOCATION
    default_acl = "private"
    file_overwrite = False


class LocalMirrorStorage(FileSystemStorage):
    location = settings.LOCAL_MIRROR_LOCATION
    _instance = None

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = cls()
        return cls._instance

    def mirror_s3_file(self, s3file, internal_path=None):
        s3_file_name = s3file.name
        if internal_path:
            s3_file_name = os.path.join(internal_path, s3_file_name)
        if self.exists(s3_file_name):
            file_name = s3_file_name
        else:
            content_file = ContentFile(s3file.read())
            file_name = self.save(s3_file_name, content_file)
        return file_name


# Mechanizm pozwalający na ustalenie globalnego private storage zależnego od ustawień środowiska
if settings.PRIVATE_FILE_STORAGE_ON_S3:
    PrivateFileStorage = PrivateS3MediaStorage
else:
    PrivateFileStorage = PrivateLocalFileStorage


def get_local_file_path(file_obj):
    if not settings.PRIVATE_FILE_STORAGE_ON_S3:
        return file_obj.path
    local_file_path = LocalMirrorStorage.get_instance().mirror_s3_file(file_obj)
    return os.path.join(settings.MIRRORED_FILES_ROOT, local_file_path)
