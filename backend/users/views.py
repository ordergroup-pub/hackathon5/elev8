from django.shortcuts import render
from django.views.generic import TemplateView


# Create your views here.
class UserCertificateView(TemplateView):
    template_name = 'users/user_certificate.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['certificate'] = {
            'name': 'whatev'
        }
        return context

