#!/usr/bin/env bash

linters_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

$linters_dir/isort-fix.bash
$linters_dir/black-fix.bash
