#!/usr/bin/env bash

backend_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)

set -e # Configure shell so that if one command fails, it exits
cd $backend_dir
coverage erase
coverage report
