#!/usr/bin/env bash

backend_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)

isort $backend_dir --check
