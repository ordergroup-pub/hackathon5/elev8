#!/usr/bin/env bash

backend_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)

black $@ $backend_dir --config="$backend_dir/pyproject.toml"
