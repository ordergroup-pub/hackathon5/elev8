# -*- coding: utf-8 -*-

import os

from django.core.exceptions import ImproperlyConfigured

FAKE_NONE = "NO_VALUE"


def get_env(name, default=FAKE_NONE):
    """Get the environment variable or return exception"""
    if name in os.environ:
        return os.environ[name]

    if default != FAKE_NONE:
        return default

    error_msg = "Set the {} env variable".format(name)
    raise ImproperlyConfigured(error_msg)


def get_env_bool(name, default=None):
    value = get_env(name, default=default)
    return value in [True, "True", "true"]


def get_env_int(name, default=0):
    value = get_env(name, default=default)
    if value in ["", None]:
        return default
    try:
        value = int(value)
    except ValueError as e:
        print(f"Incorrect value for integer env variable {name}: {value}. Assuming default: {default}. Error: {e}")
        value = default
    return value
