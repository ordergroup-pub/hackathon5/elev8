# -*- coding: utf-8 -*-
import os

import requests


def get_fargate_container_ip():
    private_ip = None
    raise NotImplementedError("The URI needs to be adjusted first")
    metadata_uri = os.environ.get("ECS_CONTAINER_METADATA_URI", "http://169.254.170.2/v2/metadata")

    try:
        resp = requests.get(metadata_uri)
        data = resp.json()
        network_data = data.get("Networks")
        awsvpc_networks = [x for x in network_data if x["NetworkMode"] == "awsvpc"]

        if len(awsvpc_networks) > 0:
            network_meta = awsvpc_networks[0]
            private_ip = network_meta["IPv4Addresses"][0]
    except Exception:
        # silently fail as we may not be in an ECS environment
        pass

    return private_ip
