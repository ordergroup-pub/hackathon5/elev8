# -*- coding: utf-8 -*-
import redis
from django.conf import settings

LOCK_EXPIRE = 60 * 60 * 2  # locks are expiring after 2 hours by default
redis_client = redis.Redis(host=settings.REDIS_HOST)


class RedisLock:
    def __init__(self, lock_name, blocking=True, timeout=LOCK_EXPIRE):
        self.timeout = timeout
        self.lock_name = lock_name
        self.blocking = blocking
        self.has_lock = False
        self.lock = redis_client.lock(self.lock_name, timeout=self.timeout)

    def __enter__(self):
        try:
            self.has_lock = self.lock.acquire(blocking=self.blocking)
        except Exception:
            pass
        return self.has_lock

    def __exit__(self, *args, **kwargs):
        if self.has_lock:
            self.lock.release()
