FROM python:3.9-slim-bullseye
MAINTAINER OrderGroup

ENV PYTHONUNBUFFERED=1 \
    POETRY_VERSION=1.2.2 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_CACHE_DIR='/var/cache/pypoetry' \
    POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1

WORKDIR /app

RUN apt-get update && apt-get install -y --no-install-recommends curl netcat

RUN curl -sSL https://install.python-poetry.org | python -

ENV PATH="${PATH}:/opt/poetry/bin"

COPY pyproject.toml poetry.lock /app/

# Install dependencies. The specific version of setuptools is required by flower (celery web interface).
RUN poetry install

COPY . /app/

EXPOSE 8080

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["runserver"]
