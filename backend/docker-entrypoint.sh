#!/bin/bash
# $0 is a script name, $1, $2, $3 etc are passed arguments
# $1 is our command
# Credits: https://rock-it.pl/how-to-write-excellent-dockerfiles/
CMD=$1
DB_PORT="${DATABASE_PORT:-5432}"

wait_for_db () {
    # Wait until postgres is ready
    if [ -z ${DATABASE_HOST+x} ]; then
        echo "DATABASE_HOST is unset, skipping check";
    else
        until nc -z $DATABASE_HOST $DB_PORT; do
            echo "$(date) - waiting for postgres... ($DATABASE_HOST:$DB_PORT)"
            sleep 3
        done
    fi
}

setup_django () {

    echo Running migrations
    python manage.py migrate --noinput --settings=$DJANGO_SETTINGS_MODULE

    echo Create default admins if needed
    if [ -z ${INITIAL_ADMINS+x} ]; then
        echo "INITIAL_ADMINS variable not set"
    else
        python manage.py create_admins --settings=$DJANGO_SETTINGS_MODULE
    fi

    echo Collecting static-files
    python manage.py collectstatic --noinput --settings=$DJANGO_SETTINGS_MODULE

    echo Create cache table
    python manage.py createcachetable --settings=$DJANGO_SETTINGS_MODULE
}

case "$CMD" in
    "deploy" )
        echo Running deployment scripts
        wait_for_db
        echo Database connected
        echo Setting up deployment
        setup_django
        echo Deployment completed
        ;;
    "runserver" )
        echo Waiting for database connection
        wait_for_db
        echo Database connected

        setup_django
        echo Django setup finished

        echo Starting gunicorn
        exec gunicorn --workers=5 --bind 0.0.0.0:8080 elev8.wsgi
        ;;
    "test" )
        echo Waiting for database connection
        wait_for_db
        echo Database connected

        echo Running tests

        poetry install
        exec python manage.py test
        echo YAY
        ;;
    "uwsgi" )
        echo Waiting for database connection
        wait_for_db
        echo Database connected
        setup_django

        echo Starting using uwsgi
        exec uwsgi --ini uwsgi.ini
        ;;

    * )
        # Run custom command. Thanks to this line we can still use
        # "docker run our_container /bin/bash" and it will work
        exec $CMD ${@:2}
        ;;
esac
